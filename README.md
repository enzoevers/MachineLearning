# Machine learning

In this repository I have several branches. Each with a different purpose.
* Branches:
    * **Matlab**
        I use Matlab to make things fast and to see the behaviour easily with the 
        plotting functionality.
    * **CNN_Vivado**
        This containthe Vivado HLS files, Vivado project and a file that installs all the required librares for petalinux. 
        For installing petalinux, follow this guide: https://www.xilinx.com/support/documentation/sw_manuals/petalinux2015_4/ug1144-petalinux-tools-reference-guide.pdf.
        The petalinux download link in the PDF doesn't work. You should download "Petalinux <version> Installer" from:
        https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools.html. 

